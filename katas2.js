function add(a, b) {
    return a+b;
}

 function multiply(a, b) {
    let product = 0;
    for(let i = 0; i < b; i++)
        product += a;

    return product;
}

function power(a, b) {
    let product = a;
    for(let i = 1; i < b; i++)
        product = multiply(product, a);

    return product;
}

function factorial(n) {
    let product = n;
    for(let i = n - 1; i > 0; i--) 
        product = multiply(product, i);
    
    return product;
}

function fibonacci(n) {
    let fibarr = [0, 1, 1, 2];

    for(let i = 3; i < n; i++)
        fibarr.push(add(fibarr[i - 1], fibarr[i]));

    return fibarr[fibarr.length - 1];
}